import numpy
import cv2


path = r'C:\Users\User\osirv_20_21\lab1\slike\baboon.bmp'
slika = cv2.imread(path)


plava = slika.copy()
zelena = slika.copy()
crvena = slika.copy()

plava[:,:,1] = 0
plava[:,:,2] = 0
cv2.imwrite("plava.jpg", plava)

zelena[:,:,0] = 0
zelena[:,:,2] = 0
cv2.imwrite("zelena.jpg", zelena)


crvena[:,:,0] = 0
crvena[:,:,1] = 0
cv2.imwrite("crvena.jpg", crvena)

