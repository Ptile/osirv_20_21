import cv2
import numpy


slika = r'C:\Users\User\osirv_20_21\lab1\slike\lenna.bmp'

Picture = cv2.imread(slika)

Vertikalna = Picture.copy()
Horizontalna = Picture.copy()
Vertikalno_Horizontalna = Picture.copy()

cv2.imshow("Original",Picture)
cv2.waitKey(0)
cv2.imshow("Vertikalna",Vertikalna[:512:2,:,:])
cv2.waitKey(0)
cv2.imshow("Horizontalna",Horizontalna[:,:512:2,:])
cv2.waitKey(0)
cv2.imshow("Vertikalno_Horizontalna",Vertikalno_Horizontalna[:512:2,:512:2,:])
cv2.waitKey(0)
