import cv2
import numpy as np
from matplotlib import pyplot as plt

#edge detection koristenjem Canny edge detector-a

image = cv2.imread('../../slike/building_2.jpg', cv2.IMREAD_GRAYSCALE)
blurred_image = cv2.GaussianBlur(image, (5,5), 0)

#za primjer lower = 0, upper = 255
edges = cv2.Canny(blurred_image, 0, 255)
cv2.imshow("L=0 U=255", edges)
cv2.waitKey(0)
cv2.imwrite("L0U255.jpg",edges)
#za primjer lower = 128, upper = 128
edges = cv2.Canny(blurred_image, 128, 128)
cv2.imshow("L=128 U=128", edges)
cv2.waitKey(0)
cv2.imwrite("L128U128.jpg",edges)

#za primjer lower = 255, upper = 255
edges = cv2.Canny(blurred_image, 255, 255)
cv2.imshow("L=255 U=255", edges)
cv2.waitKey(0)
cv2.imwrite("L255U255.jpg",edges)

#najvise detalja i rubova uvjerljivo je uhvatio Canny s LOWER = 0 i UPPER = 255
#Canny s LOWER = 128 i UPPER = 128 uhvatio je nesto mnaje rubova, ali i dalje je bio 
#dobar kontinuitet u linijama rubova, nekada je uhvatilo i neke cudne rubove
#Canney s LOWER = 255 i UPPER = 255 je najlosiji, nije pokupio toliko detaljno rubove, ali 
#se i dalje sve lijepo raspoznaje na slici