import numpy as np
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, lower, upper, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(lower, (1.0 - sigma) * v))
        upper = int(min(upper, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged

#SLUCAJ CANNY AUTODETEKCIJE ZA BOATS
boats = cv2.imread(r"C:\Users\User\osirv_20_21\lab3\slike\boats.bmp",cv2.IMREAD_GRAYSCALE)
#L=0,U=255
auto0255 = auto_canny(boats,0,255) #median lower = 92, median upper = 183
cv2.imwrite('boatsL0U255.jpg',auto0255) 

#L=128, U=128
auto128128 = auto_canny(boats,128,128) #median lower = 128, median upper = 128
cv2.imwrite('boatsL128U128.jpg',auto128128) 

#L=255, U=255
auto255255 = auto_canny(boats,255,255) #median lower = 255, median upper = 183
cv2.imwrite('boatsL255U255.jpg',auto255255) 


#SLUCAJ CANNY AUTODETEKCIJE ZA PEPPER
pepper = cv2.imread(r"C:\Users\User\osirv_20_21\lab3\slike\pepper.bmp",cv2.IMREAD_GRAYSCALE)
#L=0,U=255
auto0255 = auto_canny(pepper,0,255) #median lower = 81, median upper = 160
cv2.imwrite('pepperL0U255.jpg',auto0255) 

#L=128, U=128
auto128128 = auto_canny(pepper,128,128) #median lower = 128, median upper = 128
cv2.imwrite('pepperL128U128.jpg',auto128128) 

#L=255, U=255
auto255255 = auto_canny(pepper,255,255) #median lower = 255, median upper = 160
cv2.imwrite('pepperL255U255.jpg',auto255255) 


#SLUCAJ CANNY AUTODETEKCIJE ZA AIRPLANE
airplane = cv2.imread(r"C:\Users\User\osirv_20_21\lab3\slike\airplane.bmp",cv2.IMREAD_GRAYSCALE)
#L=0,U=255
auto0255 = auto_canny(airplane,0,255) #median lower = 134, median upper = 255
cv2.imwrite('airplaneL0U255.jpg',auto0255) 

#L=128, U=128
auto128128 = auto_canny(airplane,128,128) #median lower = 134, median upper = 128
cv2.imwrite('airplaneL128U128.jpg',auto128128) 

#L=255, U=255
auto255255 = auto_canny(airplane,255,255) #median lower = 255, median upper = 255
cv2.imwrite('airplaneL255U255.jpg',auto255255) 


#SLUCAJ CANNY AUTODETEKCIJE ZA BARBARA
barbara = cv2.imread(r"C:\Users\User\osirv_20_21\lab3\slike\barbara.bmp",cv2.IMREAD_GRAYSCALE)
#L=0,U=255
auto0255 = auto_canny(barbara,0,255) #median lower = 72, median upper = 143
cv2.imwrite('barbaraL0U255.jpg',auto0255) 
#L=128, U=128
auto128128 = auto_canny(barbara,128,128) #median lower = 128, median upper = 128
cv2.imwrite('barbaraL128U128.jpg',auto128128) 
#L=255, U=255
auto255255 = auto_canny(barbara,255,255) #median lower = 255, median upper = 143
cv2.imwrite('barbaraL255U255.jpg',auto255255) 




