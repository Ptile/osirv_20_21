import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy

img = cv2.imread('../../slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
#blocksize = 1
dst = cv2.cornerHarris(gray,1,3,0.04)
#blocksize = 3
# dst = cv2.cornerHarris(gray,3,3,0.04)
#blocksize = 5
# dst = cv2.cornerHarris(gray,5,3,0.04)



#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# threshold for an optimal value, it may vary depending on the image.
img2[dst>0.01*dst.max()]=[0,0,255]

plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img2 , 'gray')
plt.title('Detected Corners'), plt.xticks([]), plt.yticks([])
plt.show()


cv2.imwrite("cornerHarris_1.jpg",img2)
#cv2.imwrite("cornerHarris_3.jpg",img2)
#cv2.imwrite("cornerHarris_5.jpg",img2)

#blocksize 1 uopce ne daje rubove, dok blocksize 3 i 5 daju i hvataju veliku vecinu rubova
#samo sto blocksize 5 uzima i iznacava vecu povrsinu na slici da je rub