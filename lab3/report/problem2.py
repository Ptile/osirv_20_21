import cv2
import numpy as np
from matplotlib import pyplot as plt

boats = cv2.imread('../slike/BoatsColor.bmp', cv2.IMREAD_GRAYSCALE)
baboon = cv2.imread('../slike/baboon.bmp', cv2.IMREAD_GRAYSCALE)
airplane = cv2.imread('../slike/airplane.bmp', cv2.IMREAD_GRAYSCALE)

#za boats sve thresholding metode
ret,boats1 = cv2.threshold(boats,72,255,cv2.THRESH_BINARY)
ret,boats2 = cv2.threshold(boats,72,255,cv2.THRESH_BINARY_INV)
ret,boats3 = cv2.threshold(boats,72,255,cv2.THRESH_TRUNC)
ret,boats4 = cv2.threshold(boats,72,255,cv2.THRESH_TOZERO)
ret,boats5 = cv2.threshold(boats,72,255,cv2.THRESH_TOZERO_INV)

boats6 = cv2.adaptiveThreshold(boats,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
boats7 = cv2.adaptiveThreshold(boats,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)

ret1,boats8 = cv2.threshold(boats,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

boat_images = [boats,boats1,boats2,boats3,boats4,boats5,boats6,boats7,boats8]
titles = ['obicna','thresh bin','thresh bin inv','thresh trunc','thresh tozero'
          ,'thresh to zero inv','adaptive thresh mean','adaptiv thresh gauss','thresh otsu']

for i in range(8):
    plt.subplot(2,4,i+1),plt.imshow(boat_images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()

#za baboon sve thresholding metode
ret,baboon1 = cv2.threshold(baboon,72,255,cv2.THRESH_BINARY)
ret,baboon2 = cv2.threshold(baboon,72,255,cv2.THRESH_BINARY_INV)
ret,baboon3 = cv2.threshold(baboon,72,255,cv2.THRESH_TRUNC)
ret,baboon4 = cv2.threshold(baboon,72,255,cv2.THRESH_TOZERO)
ret,baboon5 = cv2.threshold(baboon,72,255,cv2.THRESH_TOZERO_INV)

baboon6 = cv2.adaptiveThreshold(baboon,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
baboon7 = cv2.adaptiveThreshold(baboon,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)

ret1,baboon8 = cv2.threshold(baboon,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

baboon_images = [baboon,baboon1,baboon2,baboon3,baboon4,baboon5,baboon6,baboon7,baboon8]
titles = ['obicna','thresh bin','thresh bin inv','thresh trunc','thresh tozero'
          ,'thresh to zero inv','adaptive thresh mean','adaptiv thresh gauss','thresh otsu']

for i in range(8):
    plt.subplot(2,4,i+1),plt.imshow(baboon_images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()


#za airplane sve thresholding metode
ret,airplane1 = cv2.threshold(airplane,72,255,cv2.THRESH_BINARY)
ret,airplane2 = cv2.threshold(airplane,72,255,cv2.THRESH_BINARY_INV)
ret,airplane3 = cv2.threshold(airplane,72,255,cv2.THRESH_TRUNC)
ret,airplane4 = cv2.threshold(airplane,72,255,cv2.THRESH_TOZERO)
ret,airplane5 = cv2.threshold(airplane,72,255,cv2.THRESH_TOZERO_INV)

airplane6 = cv2.adaptiveThreshold(airplane,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
airplane7 = cv2.adaptiveThreshold(airplane,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)

ret1,airplane8 = cv2.threshold(airplane,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

baboon_images = [airplane,airplane1,airplane2,airplane3,airplane4,
                 airplane5,airplane6,airplane7,airplane8]
titles = ['obicna','thresh bin','thresh bin inv','thresh trunc','thresh tozero'
          ,'thresh to zero inv','adaptive thresh mean','adaptiv thresh gauss','thresh otsu']

for i in range(8):
    plt.subplot(2,4,i+1),plt.imshow(baboon_images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()


