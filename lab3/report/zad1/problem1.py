import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img = cv2.imread("../../slike/airplane.bmp",0)

#GAUSSOV SUM
gauss1 = gaussian_noise(img,0,20)
showhist(gauss1)
show(gauss1)
cv2.imwrite("Gauss_20.jpg",gauss1)
gauss2 = gaussian_noise(img,0,10)
showhist(gauss2)
show(gauss2)
cv2.imwrite("Gauss_10.jpg",gauss2)

#UNIFORMNI SUM
uniform1 = uniform_noise(img,-20,20)
showhist(uniform1)
show(uniform1)
cv2.imwrite("Uniform_20.jpg",uniform1)
uniform2 = uniform_noise(img,-40,40)
showhist(uniform2)
show(uniform2)
cv2.imwrite("Uniform_40.jpg",uniform2)
uniform3 = uniform_noise(img,-60,60)
showhist(uniform3)
show(uniform3)
cv2.imwrite("Uniform_60.jpg",uniform3)

#SALT'N'PEPPER
SnP1 = salt_n_pepper_noise(img,5)
showhist(SnP1)
show(SnP1)
cv2.imwrite("SnP_5.jpg",SnP1)
SnP2 = salt_n_pepper_noise(img,10)
showhist(SnP2)
show(SnP2)
cv2.imwrite("SnP_10.jpg",SnP2)
SnP3 = salt_n_pepper_noise(img,15)
showhist(SnP3)
show(SnP3)
cv2.imwrite("SnP_15.jpg",SnP3)
SnP4 = salt_n_pepper_noise(img,20)
showhist(SnP4)
show(SnP4)
cv2.imwrite("SnP_20.jpg",SnP4)




