import numpy as np
import cv2

datoteka = '../../slike/BoatsColor.bmp'

slika = cv2.imread(datoteka, cv2.IMREAD_GRAYSCALE)

quantisation_interval = [1,2,3,4,5,6,7,8]

for quant in quantisation_interval:
    img = np.array(slika)
    noise = np.random.uniform(-0.5,0.5,img.shape)
    potencija = 2 ** int(quant) 
    img = img.astype(np.float32)
    img = (np.floor((img/potencija) + noise + 0.5)) * potencija
    img[ img > 255 ] = 255
    img[ img < 0 ] = 0
    img = img.astype(np.uint8)
    cv2.imwrite(('boats_' + str(quant) + 'n.bmp'),img)
    
    
#Šumovi s kvantizacijom pocinju biti opazani tek na q=3 i q=4, na q=2 jos i ne izgleda toliko
#drugacije od originalne slike
#Nakon uocavanja šumova i promjena na slici,
#na svakoj iducoj kvantizacijskoj razini slika se bitno pogorsava
#uz dodane šumove
    