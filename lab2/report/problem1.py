import numpy as np
import cv2

def dodavanje_okvira(sirina_slike, slika):
    slika[0:10,0:sirina_slike] = 0
    slika[:,0:10] = 0
    slika[-10:,0:sirina_slike] = 0
    slika[:,sirina_slike-10:sirina_slike] = 0
    
    return slika



prva_slika = cv2.imread('../slike/airplane.bmp')
druga_slika = cv2.imread('../slike/baboon.bmp')
treca_slika = cv2.imread('../slike/lenna.bmp')

prva_izrezana = prva_slika[0:400,:]
druga_izrezana = druga_slika[0:400,:]
treca_izrezana = treca_slika[0:400,:]


spojena_slika = np.hstack((prva_izrezana,druga_izrezana,treca_izrezana))

okvir_slika = cv2.copyMakeBorder(spojena_slika,20,20,20,20,cv2.BORDER_CONSTANT)


cv2.imshow('spojena slika', spojena_slika)
cv2.waitKey(0)
cv2.imshow('spojena slika s okvirom', okvir_slika)
cv2.waitKey(0)