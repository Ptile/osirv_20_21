import numpy as np
import cv2

datoteka = '../../slike/BoatsColor.bmp'

slika = cv2.imread(datoteka, cv2.IMREAD_GRAYSCALE)

quantisation_interval = [1,2,3,4,5,6,7,8]

for quant in quantisation_interval:
    img = np.array(slika)
    potencija = 2 ** int(quant) 
    img = img.astype(np.float32)
    img = (np.floor(img/potencija + 0.5) * potencija)
    img[ img > 255 ] = 255
    img[ img < 0 ] = 0
    img = img.astype(np.uint8)
    cv2.imwrite(('boats_' + str(quant) + '.bmp'),img)
    
    
#Na slici prilikom kvantizacije se najpriije vide promjene na nebu. 
#To se odmah prepozna na q=2 i q=3
#Nadalje, u svim ostalim slikama se polako pocinju viđati promjene i na brodu i u okolini. 