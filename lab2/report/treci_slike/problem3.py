import numpy as np
import cv2


slika_jedan = cv2.imread('../../slike/airplane.bmp', cv2.IMREAD_GRAYSCALE)
slika_dva = cv2.imread('../../slike/baboon.bmp', cv2.IMREAD_GRAYSCALE)
slika_tri = cv2.imread('../../slike/barbara.bmp', cv2.IMREAD_GRAYSCALE)
slika_cetiri = cv2.imread('../../slike/BoatsColor.bmp', cv2.IMREAD_GRAYSCALE)
slika_pet = cv2.imread('../../slike/dioslike.bmp', cv2.IMREAD_GRAYSCALE)
slika_sest = cv2.imread('../../slike/goldhill.bmp', cv2.IMREAD_GRAYSCALE)
slika_sedam = cv2.imread('../../slike/lenna.bmp', cv2.IMREAD_GRAYSCALE)
slika_osam = cv2.imread('../../slike/hp_logo.bmp', cv2.IMREAD_GRAYSCALE)


slika_jedan_invert = cv2.bitwise_not(slika_jedan) 
slika_dva_invert = cv2.bitwise_not(slika_dva) 
slika_tri_invert = cv2.bitwise_not(slika_tri) 
slika_cetiri_invert = cv2.bitwise_not(slika_cetiri) 
slika_pet_invert = cv2.bitwise_not(slika_pet) 
slika_sest_invert = cv2.bitwise_not(slika_sest) 
slika_sedam_invert = cv2.bitwise_not(slika_sedam) 
slika_osam_invert = cv2.bitwise_not(slika_osam) 



cv2.imwrite("airplane_inverted.jpg", slika_jedan_invert)
cv2.imwrite("baboon_inverted.jpg", slika_dva_invert)
cv2.imwrite("barbara_inverted.jpg", slika_tri_invert)
cv2.imwrite("BoatsColor_inverted.jpg", slika_cetiri_invert)
#cv2.imwrite("dioslike_inverted.jpg", slika_pet_invert)
cv2.imwrite("goldhill_inverted.jpg", slika_sest_invert)
cv2.imwrite("lenna_inverted.jpg", slika_sedam_invert)
#cv2.imwrite("hp_logo_inverted.jpg", slika_osam_invert)





cv2.imshow("invert", slika_jedan_invert)
cv2.waitKey(0)
