import numpy as np
import cv2


slika_jedan = cv2.imread('../../slike/airplane.bmp', cv2.IMREAD_GRAYSCALE)
slika_dva = cv2.imread('../../slike/baboon.bmp', cv2.IMREAD_GRAYSCALE)
slika_tri = cv2.imread('../../slike/barbara.bmp', cv2.IMREAD_GRAYSCALE)
slika_cetiri = cv2.imread('../../slike/BoatsColor.bmp', cv2.IMREAD_GRAYSCALE)
slika_pet = cv2.imread('../../slike/dioslike.bmp', cv2.IMREAD_GRAYSCALE)
slika_sest = cv2.imread('../../slike/goldhill.bmp', cv2.IMREAD_GRAYSCALE)
slika_sedam = cv2.imread('../../slike/lenna.bmp', cv2.IMREAD_GRAYSCALE)
slika_osam = cv2.imread('../../slike/hp_logo.bmp', cv2.IMREAD_GRAYSCALE)

threshold_list = [63,127,191]

for threshold in threshold_list:
    new_pic = slika_jedan.copy()
    new_pic[new_pic < threshold] = 0
    cv2.imwrite(('airplane_' + str(threshold) + '_thresh.png'),new_pic )

for threshold in threshold_list:
    new_pic = slika_dva.copy()
    new_pic[new_pic < threshold] = 0
    cv2.imwrite(('baboon_' + str(threshold) + '_thresh.png'),new_pic )
    
for threshold in threshold_list:
    new_pic = slika_tri.copy()
    new_pic[new_pic < threshold] = 0
    cv2.imwrite(('barbara_' + str(threshold) + '_thresh.png'),new_pic )
    
for threshold in threshold_list:
    new_pic = slika_cetiri.copy()
    new_pic[new_pic < threshold] = 0
    cv2.imwrite(('BoatsColor_' + str(threshold) + '_thresh.png'),new_pic )
    
for threshold in threshold_list:
    new_pic = slika_sest.copy()
    new_pic[new_pic < threshold] = 0
    cv2.imwrite(('goldhill_' + str(threshold) + '_thresh.png'),new_pic )
    
    
for threshold in threshold_list:
    new_pic = slika_sedam.copy()
    new_pic[new_pic < threshold] = 0
    cv2.imwrite(('lenna_' + str(threshold) + '_thresh.png'),new_pic )



cv2.imshow('slika', slika_jedan)
cv2.waitKey(0)
