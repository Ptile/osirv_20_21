import cv2
import numpy as np
from math import sin, cos, pi

datoteka = '../../slike/baboon.bmp'

img = cv2.imread(datoteka,cv2.IMREAD_GRAYSCALE)
umanjena_slika = cv2.resize(img, None, fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC)
rows, cols = umanjena_slika.shape

lista_rotacija = [0,30,60,90,120,180,210,240,270,300,330,360]
dodana_slika = []

for rotacija in lista_rotacija:
    M = cv2.getRotationMatrix2D((cols/2,rows/2),rotacija,1)
    dst = cv2.warpAffine(umanjena_slika,M,(cols,rows))
    dodana_slika.append(dst)
    
slika = np.hstack(dodana_slika)
cv2.imshow("umanjena slika", slika)
cv2.waitKey(0)
cv2.imwrite("baboon_rotation.bmp", slika)